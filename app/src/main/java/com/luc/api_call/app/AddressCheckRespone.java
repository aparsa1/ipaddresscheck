package com.luc.api_call.app;

import org.odata4j.core.OEntity;
import org.odata4j.core.OProperty;

import java.util.List;

/**
 * Created by aziz on 11/24/2014.
 */
  class AddressCheckResponse {

    public AddressCheckResponse()
    {}
    public AddressCheckResponse(OEntity response)
    {
        Response=response;
    }
    @Override
    public String toString()
    {
       List<OProperty<?>> properties= Response.getProperties();
        Result="";
        try
        {
            Result+= printRecordResultCodes( Response.getProperty("Results").getValue().toString())+"\n"; //First show the result of check
        }
        catch (Exception ex)
        {}

        for(OProperty<?> op: properties) //Then show other properties
        {
            try {
                if (op.getValue() != null && !op.getValue().toString().isEmpty()) {

                  if(op.getName().equals("Results"))
                  {

                  }
                    else {
                      Result += op.getName() + ":" + op.getValue().toString() + "\n";
                  }
                  }
            }
            catch (Exception ex)
            {}
        }
        return Result;
    }


    public OEntity Response;
    public String CBSATitle,CountyName,CountyFlips,TimeZoneCode,TimeZoneName;
    public String Company;

    public String Urbanization;
    String Result;
    public String Address1;

    public String Address2;

    public String Suite;

    public String PublicMailBox;

    public String City;

    public String State;

    public String Zip;

    public String Plus4;

    public String CarrierRoute;

    public String DeliveryPointCode;

    public String DeliveryPointCheckDigit;

    public String CongressionalDistrict;

    public String Type;

    public String Country;

    public String AddressKey;

    public String Parsed;


    public  String printRecordResultCodes(String resultCodes)
    {
        String[] values = resultCodes.split(",");
        String result="";
        //Do necessary work with the values, here we just print them out
        for (String str : values)
        {
            result+=str+": ";

            // Results Codes
            if (resultCodes.equals(" "))
            {
                result+="\n";
            }
            else   if (str.equals("AS01"))
            {
                result+="Address Matched to Postal Database\n";
            }
            else   if (str.equals("AS02"))
            {
                result+="Street Only Match \n";
            }
            else   if (str.equals("AS03"))
            {
                result+="Non USPS Address Match\n";
            }
            else if (str.equals("AS09"))
            {
                result+="Foreign Postal Code Detected\n";
            }
            else   if (str.equals("AS10"))
            {
                result+="Address Matched to CMRA\n";
            }
            else   if (str.equals("AS12"))
            {
                result+="Address Verified at the DPV Level\n";
            }
            else   if (str.equals("AS13"))
            {
                result+="Address Updated by LACS\n";
            }
            else  if (str.equals("AS14"))
            {
                result+="Address Updated by Suite Link\n";
            }
            else  if (str.equals("AS15"))
            {
                result+="Address Updated by Suite Finder\n";
            }
            else  if (str.equals("AS16"))
            {
                result+="Address is vacant\n";
            }
            else    if (str.equals("AS17"))
            {
                result+="Alternate delivery\n";
            }
            else   if (str.equals("AS18"))
            {
                result+="Artificially created adresses detected,DPV processing terminated at this point\n";
            }
            else    if (str.equals("AS20"))
            {
                result+="Address Deliverable by USPS only\n";
            }
            else    if (str.equals("AS23"))
            {
                result+="Extraneous information found\n";
            }
            //Change Codes
            else  if (str.equals("AC01"))
            {
                result+="ZIP Code Change\n";
            }
            else    if (str.equals("AC02"))
            {
                result+="State Change\n";
            }
            else    if (str.equals("AC03"))
            {
                result+="City Change\n";
            }
            else if (str.equals("AC04"))
            {
                result+="Base/Alternate Changed\n";
            }
            else    if (str.equals("AC05"))
            {
                result+="Alias Name Change\n";
            }
            else if (str.equals("AC06"))
            {
                result+="Address1/Address2 Swap\n";
            }
            else  if (str.equals("AC07"))
            {
                result+="Address1/Company Swap\n";
            }
            else    if (str.equals("AC08"))
            {
                result+="Plus4 Change\n";
            }
            else   if (str.equals("AC09"))
            {
                result+="Urbanization Change\n";
            }
            else if (str.equals("AC10"))
            {
                result+="Street Name Change\n";
            }
            else   if (str.equals("AC11"))
            {
                result+="Street Suffix Change\n";
            }
            else    if (str.equals("AC12"))
            {
                result+="Street Directional Change\n";
            }
            else     if (str.equals("AC13"))
            {
                result+="Suite Name Change\n";
            }

            // Error Handling
            else    if (str.equals("AE01"))
            {
                result+="Zip Code Error\n";
            }
            else    if (str.equals("AE02"))
            {
                result+="Unknown Street\n";
            }
            else   if (str.equals("AE03"))
            {
                result+="Component Error\n";
            }
            else   if (str.equals("AE04"))
            {
                result+="Non-Deliverable Address\n";
            }
            else    if (str.equals("AE05"))
            {
                result+="Address Matched to Multiple Records\n";
            }
            else   if (str.equals("AE06"))
            {
                result+="Address Matched to Early Warning System\n";
            }
            else   if (str.equals("AE07"))
            {
                result+="Empty Address Input\n";
            }
            else    if (str.equals("AE08"))
            {
                result+="Suite Range Error\n";
            }
            else      if (str.equals("AE09"))
            {
                result+="Suite Range Missing\n";
            }
            else    if (str.equals("AE010"))
            {
                result+="Primary Range Error\n";
            }
            else    if (str.equals("AE011"))
            {
                result+="PRimary Range Missing\n";
            }
            else     if (str.equals("AE012"))
            {
                result+="PO or RR Box Number Error\n";
            }
            else    if (str.equals("AE013"))
            {
                result+="PO or RR Box Number Missing\n";
            }
            else     if (str.equals("AE014"))
            {
                result+="Input Address Matched to CMRA but Secondary Number not Present\n";
            }
            else   if (str.equals("AE017"))
            {
                result+="A suite number was entered but no suite information found for primary address;\n";
            }
            else    if (str.equals("GS01"))
            {
                result+="Geocoded to Street Level\n";
            }
            else    if (str.equals("GS02"))
            {
                result+="Geocoded to the Neighborhood Level\n";
            }
            else    if (str.equals("GS03"))
            {
                result+="Geocoded to Community Level\n";
            }
            else    if (str.equals("GS05"))
            {
                result+="Geocoded to Rooftop Level\n";
            }
            else    if (str.equals("GS06"))
            {
                result+="Geocoded to Interpolated Rooftop Leveln";
            }
        }

        return result;
    }
    }


