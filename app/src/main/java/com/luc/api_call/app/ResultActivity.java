package com.luc.api_call.app;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.odata4j.consumer.ODataConsumer;
import org.odata4j.consumer.ODataConsumers;
import org.odata4j.consumer.behaviors.OClientBehaviors;
import org.odata4j.core.OEntity;
import org.odata4j.core.OQueryRequest;

import java.io.InputStream;


public class ResultActivity extends ActionBarActivity {
public final static  Address address=MainActivity.address;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Intent intent=getIntent();
     //   String message= intent.getStringExtra(MainActivity.EXTRA_MESSAGE);


        EditText txtIP=(EditText) findViewById(R.id.txtIP);
        txtIP.setText(address.IP);

        EditText txtISP=(EditText) findViewById(R.id.txtISP);
        txtISP.setText(address.ISP);

        EditText txtDomain=(EditText) findViewById(R.id.txtDomain);
        txtDomain.setText(address.Domain);

        EditText txtCity=(EditText) findViewById(R.id.txtCity);
        txtCity.setText(address.City);

        EditText txtRegion=(EditText) findViewById(R.id.txtRegion);
        txtRegion.setText(address.Region);

        EditText txtZip=(EditText) findViewById(R.id.txtZip);
        txtZip.setText(address.ZipCode);

        EditText txtCountry=(EditText) findViewById(R.id.txtCountry);
        txtCountry.setText(address.CountryAbbreviation);

        EditText txtLatitude=(EditText) findViewById(R.id.txtLatitude);
        txtLatitude.setText(address.Latitude);

        EditText txtLongitude=(EditText) findViewById(R.id.txtLongitude);
        txtLongitude.setText(address.Longitude);

        TextView txtMessage=(TextView) findViewById(R.id.lblMessage);
        txtMessage.setText(address.Message);
     //   setContentView(R.layout.activity_result);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.result, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.btnAddressVerifier) {

            return true;
        }
        else if(id==R.id.btnIpLocator)
        {

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void VerifyAddress(View view) {

        EditText txtISP = (EditText)findViewById(R.id.txtISP);
        String ISP=txtISP.getText().toString();

        EditText txtCity=(EditText) findViewById(R.id.txtCity);
        String city=txtCity.getText().toString();

        EditText txtRegion=(EditText) findViewById(R.id.txtRegion);
        String region=txtRegion.getText().toString();

        EditText txtZip=(EditText)findViewById(R.id.txtZip);
        String zip=txtZip.getText().toString();

        EditText txtCountry=(EditText)findViewById(R.id.txtCountry);
        String country=txtCountry.getText().toString();
        if(!city.isEmpty() || !region.isEmpty() || !zip.isEmpty() )
        {
           String SAdd=ISP+" "+city+" "+region+" "+zip+" "+country;
            new CallAPI().execute(SAdd);
            Toast.makeText(this, "Verifying Address ...",Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this,"Enter your Address",Toast.LENGTH_SHORT).show();
        }
    }

    private class CallAPI extends AsyncTask<String,String,String>
    {
        @Override
        protected String doInBackground(String... params)
        {

            String SAdd=params[0];//SAdd to call

            String resultToDisplay="";
            InputStream inputStream=null;

            try {
                ODataConsumer c = ODataConsumers
                        .newBuilder("https://api.datamarket.azure.com/Data.ashx/MelissaData/AddressCheck/v1/")
                        .setClientBehaviors(OClientBehaviors.basicAuth("accountKey", "zGVk/N0JqBlflG2xGX+rR9JYPuZ7tNIKguGRGvu1OxY"))
                        .build();
                OQueryRequest<OEntity> oRequest = c.getEntities("SuggestAddresses")
                        .custom("Address", "'"+SAdd+"'")
                        .custom("MaximumSuggestions", "3")
                        .custom("MinimumConfidence", "0");
                int i=0;
             for (OEntity en: oRequest.execute())
             {
                 resultToDisplay+="Record:"+1+"\n";
                 AddressCheckResponse addressCheckResponse=new AddressCheckResponse(en);
                 resultToDisplay+=addressCheckResponse.toString();
             }
                return resultToDisplay;

            }
            catch(Exception e )
            {

                return e.getMessage();
            }

        }


        @Override
        protected void onPostExecute(String result){
           // Intent intent = new Intent(getApplicationContext(),ResultActivity.class);
            // intent.putExtra(EXTRA_MESSAGE,result);
           // startActivity(intent);
            TextView lblMessage=(TextView)findViewById(R.id.lblMessage);
            lblMessage.setText(result);
        }

    }
}
