package com.luc.api_call.app;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
//import org.core4j.Enumerable;

//import org.core4j.Enumerable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odata4j.consumer.ODataConsumer;
import org.odata4j.consumer.ODataConsumers;
import org.odata4j.consumer.behaviors.OClientBehaviors;
import org.odata4j.core.OEntity;
import org.odata4j.core.OQueryRequest;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class MainActivity extends ActionBarActivity {

    public final static String apiURL="https://api.datamarket.azure.com/Data.ashx/MelissaData/IPCheck/v1/";
    public final static String EXTRA_MESSAGE="com.luc.api_call.app.MESSAGE";
    public final static Address address=new Address();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.btnAddressVerifier) {

            Intent intent = new Intent(getApplicationContext(),ResultActivity.class);
            startActivity(intent);
            return true;
        }
        else if(id==R.id.btnIpLocator)
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void LocateIP(View view)
    {
        EditText txtIP = (EditText)findViewById(R.id.txtIP);
        String IP=txtIP.getText().toString();

        if(IP !=null && !IP.isEmpty())
        {
          Toast.makeText(this, "Fetching Data",Toast.LENGTH_SHORT);
          new CallAPI().execute(IP);
            Toast.makeText(this, "Fetching Data...",Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this,"Enter your URL",Toast.LENGTH_SHORT).show();
        }
    }

    private class CallAPI extends AsyncTask<String,String,String>
    {
      @Override
        protected String doInBackground(String... params)
        {

            String ip=params[0];//URL to call
            address.IP=ip;
            String resultToDisplay="";
            InputStream inputStream=null;

            try {
                ODataConsumer c = ODataConsumers.newBuilder("https://api.datamarket.azure.com/Data.ashx/MelissaData/IPCheck/v1/").setClientBehaviors(OClientBehaviors.basicAuth("accountKey", "zGVk/N0JqBlflG2xGX+rR9JYPuZ7tNIKguGRGvu1OxY")).build();


               OQueryRequest<OEntity> oRequest = c.getEntities("SuggestIPAddresses").custom("IPAddress", "'"+ip+"'") .custom("MaximumSuggestions", "5") .custom("MinimumConfidence", "0.7");
                int i=0;
                for(OEntity en :  oRequest.execute())
                {


                    address.ISP=en.getProperty("ISP").getValue().toString();
                    address.Domain=en.getProperty("Domain").getValue().toString();
                    address.City=en.getProperty("City").getValue().toString();
                    address.Region=en.getProperty("Region").getValue().toString();
                    address.ZipCode=en.getProperty("ZipCode").getValue().toString();
                    address.Country=en.getProperty("Country").getValue().toString();
                    address.CountryAbbreviation=en.getProperty("CountryAbbreviation").getValue().toString();
                    address.Latitude=en.getProperty("Latitude").getValue().toString();
                    address.Longitude=en.getProperty("Longitude").getValue().toString();
                    i++;
                }

                address.Message="Number of addresses found"+i;
                return resultToDisplay;

            }
            catch(Exception e )
            {
               address.Message=e.getMessage();
               return e.getMessage();
            }

        }


        @Override
        protected void onPostExecute(String result){
            Intent intent = new Intent(getApplicationContext(),ResultActivity.class);
           // intent.putExtra(EXTRA_MESSAGE,result);
            startActivity(intent);

        }

    }



}
